import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class RegScreen extends StatefulWidget {
  @override
  _RegScreenState createState() => _RegScreenState();
}

class _RegScreenState extends State<RegScreen> {
    TextEditingController emailController = new TextEditingController();
    TextEditingController userController = new TextEditingController();
    TextEditingController passwordController = new TextEditingController();
    final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

    bool isLoding = false;


    signup  (String name,String email,String pass) async{
        SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
        if(name.isEmpty){
            final snackBar = SnackBar(
                content: Text('Yay! A SnackBar! $name'),
                backgroundColor: Colors.red,
                duration: Duration(seconds: 2),
            );
            _scaffoldKey.currentState.showSnackBar(snackBar);
        }else if(email.isEmpty){
            final snackBar = SnackBar(
                content: Text('Yay! A SnackBar! $email'),
                backgroundColor: Colors.red,
                duration: Duration(seconds: 2),
            );
            _scaffoldKey.currentState.showSnackBar(snackBar);
        }else if(pass.isEmpty){
            final snackBar = SnackBar(
                content: Text('Yay! A SnackBar! $pass'),
                backgroundColor: Colors.red,
                duration: Duration(seconds: 2),
            );
            _scaffoldKey.currentState.showSnackBar(snackBar);

        }else{
            var jsonResponse = null;
            Map register = {
                'name' :name,
                'email' :email,
                'password' : pass
            };
            var url ='http://10.0.2.2:8000/api/';
            var response = await http.post(url+'register',body: register);
            if(response.statusCode == 200){
                jsonResponse = json.decode(response.body);
                print(jsonResponse);
                print('this is cpplll');
                print(jsonResponse['token']);
                setState(() {
                    isLoding = false;
                });
                sharedPreferences.setString("token", jsonResponse['token']);
            }
        }
    }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
            title: Text('Registraion'),
        ),
        body: isLoding ? Center(child: CircularProgressIndicator(),): Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                    TextFormField(
                        controller: userController,
                        decoration: InputDecoration(
                            hintText: 'UserName',
                            icon: Icon(Icons.supervised_user_circle)
                        ),

                    ),
                    TextFormField(
                        controller: emailController,
                        decoration: InputDecoration(
                            hintText: 'Email',
                            icon: Icon(Icons.email)
                        ),

                    ),
                    TextFormField(
                        controller: passwordController,
                        decoration: InputDecoration(
                            hintText: 'Password',
                            icon: Icon(Icons.lock)
                        ),

                    ),
                    Container(
                        child: RaisedButton(
                            color: Colors.purple,
                            child: Text('register'),
                            onPressed: (){
                                setState(() {
                                    isLoding = true;
                                });
                                signup(userController.text,emailController.text,passwordController.text);
//                         natok();
                            },
                        ),
                    ),
                ],
            ),
        ),
    );
  }
}
