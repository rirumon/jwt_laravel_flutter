import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app_jwt/reg_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
    TextEditingController emailController = new TextEditingController();
    TextEditingController passwordController = new TextEditingController();
    final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

    bool isLoding = false;


    signin  (String email,String pass) async{
        SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
        if(email.isEmpty){
            final snackBar = SnackBar(
                content: Text('Yay! A SnackBar! $email'),
                backgroundColor: Colors.red,
                duration: Duration(seconds: 2),
            );
            _scaffoldKey.currentState.showSnackBar(snackBar);
        }else if(pass.isEmpty){
            final snackBar = SnackBar(
                content: Text('Yay! A SnackBar! $pass'),
                backgroundColor: Colors.red,
                duration: Duration(seconds: 2),
            );
            _scaffoldKey.currentState.showSnackBar(snackBar);

        }else{
            var jsonResponse = null;
            Map login = {

                'email' :email,
                'password' : pass
            };
            var url ='http://10.0.2.2:8000/api/';
            var response = await http.post(url+'register',body: login);
            if(response.statusCode == 200){
                jsonResponse = json.decode(response.body);
                print(jsonResponse);
                print('this is cpplll');
                print(jsonResponse['token']);
                setState(() {
                    isLoding = false;
                });
                sharedPreferences.setString("token", jsonResponse['token']);
            }
        }
    }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
            title: Text('Login Screen'),
        ),
        body: SafeArea(
            child: SingleChildScrollView(
              child: Container(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                          //login form
                          Container(
                              child: Column(
                                  children: <Widget>[
                                      TextFormField(
                                          controller: emailController,
                                          decoration: InputDecoration(
                                              hintText: 'Email',
                                              icon: Icon(Icons.email)
                                          ),

                                      ),
                                      TextFormField(
                                          controller: passwordController,
                                          decoration: InputDecoration(
                                              hintText: 'Password',
                                              icon: Icon(Icons.lock)
                                          ),

                                      ),
                                      Container(
                                          child: RaisedButton(
                                              color: Colors.purple,
                                              child: Text('register'),
                                              onPressed: (){
                                                  setState(() {
                                                      isLoding = true;
                                                  });
                                                  signin(emailController.text,passwordController.text);
//                         natok();
                                              },
                                          ),
                                      ),
                                  ],
                              ),
                          ),
                          //registration navigation
                          Container(
                              color: Colors.purple,
                              width: double.infinity,
                              child: RaisedButton(
                                  child: Text('Sign Up'),
                                  color: Colors.pink,
                                  onPressed: (){
                                      Navigator.push(context, MaterialPageRoute(builder: (context) => RegScreen()));
                                  },
                              ),
                          ),
                      ],
                  ),
              ),
            ),
        ),
    );
  }
}
